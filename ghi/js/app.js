function convertDate(longDate) {
    const dateObject = new Date(longDate)
    const month = dateObject.getMonth()
    const day = dateObject.getDate()
    const year = dateObject.getFullYear()

    return `${month}/${day}/${year}`

}

function createCard(name, description, pictureUrl, start, end, location) {
    return `
        <div class="card shadow-sm p-3 mb-5 bg-white rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                <p>${start} - ${end}</p>
            </div>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            return `<div class="alert alert-danger" role="alert">
            Something went wrong :(
            </div>`
        } else {
            const data = await response.json()



            // const conference = data.conferences[0]
            // const nameTag = document.querySelector('.card-title')
            // nameTag.innerHTML = conference.name

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)
                if (detailResponse.ok) {
                    const details = await detailResponse.json()
                    const title = details.conference.name
                    const description = details.conference.description
                    const pictureUrl = details.conference.location.picture_url
                    const start = convertDate(details.conference.starts)
                    const end = convertDate(details.conference.ends)
                    const location = details.conference.location.name
                    const html = createCard(title, description, pictureUrl, start, end, location)
                    const column = document.querySelector('.col');
                    column.innerHTML += html;
                }



                    // const conferenceDescription = details.conference["description"]
                    // const descriptionTag = document.querySelector('.card-text')
                    // descriptionTag.innerHTML = conferenceDescription

                    // const image = details.conference.location.picture_url
                    // const imageTag = document.querySelector('.card-img-top')
                    // imageTag.src = image
                    // console.log(image)
            }
        }
    } catch (e) {
      // Figure out what to do if an error is raised
    }

});
